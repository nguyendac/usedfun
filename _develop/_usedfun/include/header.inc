<div class="header_top show_pc">
  <div class="header_top_in">
    <div class="row">
      <div class="header_top_r">
        <div class="header_top_r_user">
          <div>
            <p>こんにちは、<span>ゲスト 様</span></p>
            <p>ご利用可能ポイント <span>0</span> pt</p>
          </div>
        </div>
        <ul class="func_shop">
          <li><a href=""><i class="ic_deal"></i></a></li>
          <li><a href=""><i class="ic_sale"></i></a></li>
          <li><a href=""><i class="ic_chat"></i></a></li>
          <li><a href=""><i class="ic_heart"></i></a></li>
        </ul>
        <a href=""><i class="ic_shop"></i></a>
      </div>
      <div class="header_top_fr show_pc">
        <form class="" action="">
          <input type="text">
          <button type="submit"></button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="header_bot">
  <div class="row">
    <h1 class="show_pc ttl1"></h1>
    <div class="df">
      <div class="logo">
        <div class="logo_l">

          <a class="link_logo" href="">
            <img src="/assets/img/logo.png" alt="UsedFun">
          </a>
        </div>
        <div class="logo_r">
          <ul class="func_shop">
            <li><a href=""><i class="ic_deal"></i></a></li>
          </ul>
          <a href=""><i class="ic_shop"></i></a>
          <span class="menu_icon" id="menu_icon"></span>
        </div>
      </div>
      <nav class="header_nav" id="header_nav">
        <p class="show_sp">こんにちは、ゲスト様
          <br>ご利用可能ポイント0ポイント</p>
        <ul class="show_sp">
          <li><a href="">新規会員登録/ログイン</a></li>
          <li><a href="">セール</a></li>
          <li><a href="">お知らせ</a></li>
          <li><a href="">お気に入り一覧ご</a></li>
          <li><a href="">買い物か</a></li>
        </ul>
        <div class="header_top_fr show_sp">
          <form class="" action="">
            <input type="text">
            <button type="submit"></button>
          </form>
        </div>
        <p class="mt show_sp">ITEM</p>
        <ul>
          <li><a href="">デスクトップパソコン</a></li>
          <li><a href="">ノートパソコン</a></li>
          <li><a href="">ゲーミングPC</a></li>
          <li><a href="">周辺機器</a></li>
          <li><a href="">複数台購入</a></li>
        </ul>
      </nav>
    </div>

  </div>
</div>