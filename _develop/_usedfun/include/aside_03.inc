<div class="aside_guide">
  <h3>ご利用ガイド</h3>
  <ul>
    <li class="active"><a href="">Used Funの強み</a></li>
    <li><a href="">配送方法と送料について</a></li>
    <li><a href="">ポイントの利用方法について</a></li>
    <li><a href="">お支払い方法について</a></li>
    <li><a href="">返品／交換について</a></li>
    <li><a href="">購入／納品の書類について</a></li>
    <li><a href="">その他よくあるご質問</a></li>
  </ul>
</div>