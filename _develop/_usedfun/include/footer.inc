<div class="row">
  <div class="footer_l">
    <div class="footer_l_main">
      <a class="footer_l_logo" href="/">
        <img src="/assets/img/logo_ft.png" alt="">
        <span>中古パソコン通販ショップ</span>
      </a>
      <p>グローバル商事株式会社<br>〒173-0026<br>東京都板橋区中丸町29-1<br>03-5926-8963</p>
      <p>営業時間：平日11:00 - 18:00</p>
      <p>古物取扱許可番号<br>東京都公安委員会許可 第305511009284</p>
    </div>
  </div>
  <div class="footer_r">
    <p class="footer_r_head">中古パソコン通販専門ショップ TOP</p>
    <div class="footer_r_link">
      <dl>
        <dt>商品一覧</dt>
        <dd>
          <ul>
            <li><a href="">店長おすすめ中古パソコン</a></li>
            <li><a href="">中古デスクトップパソコン</a></li>
            <li><a href="">中古ノートパソコン</a></li>
            <li><a href="">新品デスクトップパソコン</a></li>
            <li><a href="">新品ノートパソコン</a></li>
            <li><a href="">ジャンク品</a></li>
            <li><a href="">周辺機器</a></li>
            <li><a href="">価格別中古パソコン</a></li>
            <li><a href="">メーカー別中古パソコン</a></li>
          </ul>
        </dd>
      </dl>
      <dl>
        <dt>ご利用ガイド</dt>
        <dd>
          <ul>
            <li><a href="">Used Funの強み</a></li>
            <li><a href="">配送方法と送料について</a></li>
            <li><a href="">ポイントの利用方法について</a></li>
            <li><a href="">お支払い方法について</a></li>
            <li><a href="">返品／交換について</a></li>
            <li><a href="">購入／納品の書類について</a></li>
            <li><a href="">その他よくあるご質問</a></li>
            <li><a href="">商品のお取り扱いについて</a></li>
            <li><a href="">法人さまのご注文について</a></li>
          </ul>
          <ul>
            <li><a href="">お知らせ</a></li>
            <li><a href="">運営会社概要</a></li>
            <li><a href="">会員規約</a></li>
            <li><a href="">プライバシーポリシー</a></li>
            <li><a href="">特定商取引法に基づく表記</a></li>
            <li><a href="">お問い合わせ</a></li>
          </ul>
        </dd>
      </dl>
    </div>
    <p class="copyright">COPYRIGHT © GLOBAL, ALL RIGHTS RESERVED.</p>
  </div>
</div>