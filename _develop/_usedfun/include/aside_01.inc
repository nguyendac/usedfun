<div class="aside_banner01">
  <p>取扱商品<span>1922</span>点</p>
  <a href="">全商品を見る</a>
</div>
<div class="aside_banner02">
  <div class="aside_banner02_in">
    <img src="/assets/img/ic_diadem.png" alt="">
    <p>全商品動作検証済み、<br> 中古商品3ヶ月保証、新品１年保証付き</p>
  </div>
</div>
<div class="aside_item">
  <h3 class="aside_item_tt">カテゴリーから探す</h3>
  <div class="aside_item_ex">
    <h4><a href="">Windows 10</a></h4>
    <ul class="aside_item_ex_child">
      <li><a href="">ノートパソコン</a></li>
      <li><a href="">デスクトップパソコン</a></li>
      <li><a href="">ミニパソコン</a></li>
    </ul>
  </div>
  <div class="aside_item_ex">
    <h4><a href="">Windows 8</a></h4>
    <ul class="aside_item_ex_child">
      <li><a href="">ノートパソコン</a></li>
      <li><a href="">デスクトップパソコン</a></li>
      <li><a href="">ミニパソコン</a></li>
    </ul>
  </div>
  <div class="aside_item_ex">
    <h4><a href="">Windows 7</a></h4>
    <ul class="aside_item_ex_child">
      <li><a href="">ノートパソコン</a></li>
      <li><a href="">デスクトップパソコン</a></li>
      <li><a href="">ミニパソコン</a></li>
    </ul>
  </div>
  <div class="aside_item_ex">
    <h4><a href="">Mac OS</a></h4>
    <ul class="aside_item_ex_child">
      <li><a href="">ノートパソコン</a></li>
      <li><a href="">デスクトップパソコン</a></li>
      <li><a href="">ミニパソコン</a></li>
    </ul>
  </div>
</div>
<div class="aside_item style02">
  <h3 class="aside_item_tt">はじめてご利用される方へ</h3>
  <dl>
    <dt class="">Used Funで買うメリット</dt>
    <dd>
      <a class="ic_bag" href="">
        <span>専門バイヤーが<br> 豊富な品揃えを用意</span>
      </a>
    </dd>
  </dl>
  <dl>
    <dt><a href="">配送方法と送料</a></dt>
    <dd>
      <a class="ic_car" href="">
        <span>自社の指定便で発送<br> 本州と北海道は送料無料<br> 沖縄、離島は1,700円</span>
      </a>
    </dd>
  </dl>
  <dl>
    <dt><a href="">会員ポイントの利用</a></dt>
    <dd>
      <a class="ic_money" href="">
        <span>自社の指定便で発送<br> 本州と北海道は送料無料<br> 沖縄、離島は1,700円</span>
      </a>
    </dd>
  </dl>
  <dl>
    <dt><a href="">支払い方法</a></dt>
    <dd>
      <a class="ic_card" href="">
        <span>クレジットカード・<br> ネット決済、代金引換<br> などをご用意</span>
      </a>
    </dd>
  </dl>
  <dl>
    <dt><a href="">商品の返品・交換</a></dt>
    <dd>
      <a class="ic_contact" href="">
        <span>まずは電話かメールで<br> ご連絡ください</span>
      </a>
    </dd>
  </dl>
  <dl>
    <dt><a href="">購入や納品に関する書類</a></dt>
    <dd>
      <a class="ic_paper" href="">
        <span>お見積書や領収書、<br> 請求書なども発行します</span>
      </a>
    </dd>
  </dl>
  <dl>
    <dt><a href="">そのほかの疑問</a></dt>
    <dd>
      <a class="ic_ques" href="">
        <span>「よくあるご質問」へ</span>
      </a>
    </dd>
  </dl>
</div>
<div class="aside_item style02 style03">
  <h3 class="aside_item_tt">はじめてご利用される方へ</h3>
  <dl>
    <dt class="">会社の備品を揃えるetc…</dt>
    <dd>
      <a class="ic_bulding" href="">
        <span>大量商品のご注文、 <br> お見積りOK <br> お気軽にお問合せください</span>
      </a>
    </dd>
  </dl>
</div>