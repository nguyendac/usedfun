window.addEventListener('DOMContentLoaded', function() {
  new Modal();
})
// Modal
var Modal = function() {
  md = this;
  md._closeModal = document.querySelectorAll('.closeModal');
  md._call_modal = document.querySelectorAll('a.btn_modal[href^="#"]');
  md.boxoverlay = document.createElement('div');
  md.boxoverlay.className = 'boxoverlay';
  document.body.appendChild(md.boxoverlay);
  md._hash = null;
  md.init();
}
Modal.prototype = {
  init: function() {
    for (i = 0; i < md._call_modal.length; i = i + 1) {
      md._call_modal[i].addEventListener('click', function(e) {
        md._hash = e.currentTarget.getAttribute('href').replace('#', '');
        console.log(md._hash);
        e.preventDefault();
        md.openModal(md._hash);
      });
    };
    document.getElementsByClassName('boxoverlay')[0].addEventListener('click', function() {
      md.closeModal(md._hash);
    });
    document.addEventListener('keydown', function(e) {
      if (e.keyCode === 27) {
        md.closeModal(md._hash);
      }
    })
    for (i = 0; i < md._closeModal.length; i = i + 1) {
      md._closeModal[i].addEventListener('click', function(e) {
        e.preventDefault();
        md.closeModal(md._hash);
      });
    };
  },
  openModal: function(hc) {
    console.log(hc);
    md.boxoverlay.style.visibility = 'visible';
    md.boxoverlay.style.opacity = '1';
    document.getElementById(hc).classList.add('open');
  },
  closeModal: function(hc) {
    md.boxoverlay.style.visibility = 'hidden';
    md.boxoverlay.style.opacity = '0';
    document.getElementById(hc).classList.remove('open');
  }
}
window.addEventListener('DOMContentLoaded', function() {
  if (window.jQuery) window.Velocity = window.jQuery.fn.velocity;
  // scroll custom box
  (function() {
    var scrollContainer = document.querySelector('.scrollable'),
      scrollContentWrapper = document.querySelector('.scrollable .content-wrapper'),
      scrollContent = document.querySelector('.scrollable .content'),
      contentPosition = 0,
      scrollerBeingDragged = false,
      scroller,
      topPosition,
      scrollerHeight;

    function calculateScrollerHeight() {
      // *Calculation of how tall scroller should be
      var visibleRatio = scrollContainer.offsetHeight / scrollContentWrapper.scrollHeight;
      return visibleRatio * scrollContainer.offsetHeight;
    }

    function moveScroller(evt) {
      // Move Scroll bar to top offset
      var scrollPercentage = evt.target.scrollTop / scrollContentWrapper.scrollHeight;
      topPosition = scrollPercentage * (scrollContainer.offsetHeight); // 5px arbitrary offset so scroll bar doesn't move too far beyond content wrapper bounding box
      scroller.style.top = topPosition + 'px';
    }

    function startDrag(evt) {
      normalizedPosition = evt.pageY;
      contentPosition = scrollContentWrapper.scrollTop;
      scrollerBeingDragged = true;
    }

    function stopDrag(evt) {
      scrollerBeingDragged = false;
    }

    function scrollBarScroll(evt) {
      if (scrollerBeingDragged === true) {
        var mouseDifferential = evt.pageY - normalizedPosition;
        var scrollEquivalent = mouseDifferential * (scrollContentWrapper.scrollHeight / scrollContainer.offsetHeight);
        scrollContentWrapper.scrollTop = contentPosition + scrollEquivalent;
      }
    }

    function createScroller() {
      // *Creates scroller element and appends to '.scrollable' div
      // create scroller element
      scroller = document.createElement("div");
      scroller.className = 'scroller';
      // determine how big scroller should be based on content
      scrollerHeight = calculateScrollerHeight();
      if (scrollerHeight / scrollContainer.offsetHeight < 1) {
        // *If there is a need to have scroll bar based on content size
        scroller.style.height = scrollerHeight + 'px';
        // append scroller to scrollContainer div
        scrollContainer.appendChild(scroller);
        // show scroll path divot
        scrollContainer.className += ' showScroll';
        // attach related draggable listeners
        scroller.addEventListener('mousedown', startDrag);
        window.addEventListener('mouseup', stopDrag);
        window.addEventListener('mousemove', scrollBarScroll)
      }
    }
    createScroller();
    // *** Listeners ***
    scrollContentWrapper.addEventListener('scroll', moveScroller);
  }());
});