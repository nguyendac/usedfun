<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) EC-CUBE CO.,LTD. All Rights Reserved.
 *
 * http://www.ec-cube.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Controller;

use function Couchbase\defaultDecoder;
use Eccube\Entity\Master\ProductStatus;
use Eccube\Repository\ProductRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

class TopController extends AbstractController
{

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/", name="homepage")
     * @Template("index.twig")
     */
    public function index()
    {
        $countProduct = $this->countProduct();

        $topPorducts = $this->productRepository->createQueryBuilder('p')
            ->andWhere('p.Status = :Status')
            ->orderBy('p.update_date', 'DESC')
            ->getQuery()
            ->setParameter('Status', ProductStatus::DISPLAY_SHOW)
            ->setMaxResults(4)
            ->getResult();

        return [
            "topProducts" => $topPorducts,
            "countProduct" => $countProduct
        ];
    }

    /**
     * Count product
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countProduct()
    {
        $qb = $this->productRepository->createQueryBuilder('p')
            ->select('count(p.id)')
            ->andWhere('p.Status = :Status')
            ->setParameter('Status', ProductStatus::DISPLAY_SHOW);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
