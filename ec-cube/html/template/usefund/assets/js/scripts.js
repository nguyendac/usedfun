var swiper = new Swiper('.slider_banner', {
  effect: 'fade',
  speed: 3000,
  autoplay: true,
  slidesPerView: 1,
  loop: true,
  navigation: {
    nextEl: '.slider-button-next',
    prevEl: '.slider-button-prev',
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
});