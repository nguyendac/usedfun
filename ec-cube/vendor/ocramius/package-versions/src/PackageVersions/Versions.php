<?php

declare(strict_types=1);

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    public const ROOT_PACKAGE_NAME = 'ec-cube/ec-cube';
    public const VERSIONS          = array (
  'composer/ca-bundle' => '1.1.4@558f321c52faeb4828c03e7dc0cfe39a09e09a2d',
  'composer/composer' => '1.8.5@949b116f9e7d98d8d276594fed74b580d125c0e6',
  'composer/semver' => '1.5.0@46d9139568ccb8d9e7cdd4539cab7347568a5e2e',
  'composer/spdx-licenses' => '1.5.1@a1aa51cf3ab838b83b0867b14e56fc20fbd55b3d',
  'composer/xdebug-handler' => '1.3.2@d17708133b6c276d6e42ef887a877866b909d892',
  'doctrine/annotations' => 'v1.6.1@53120e0eb10355388d6ccbe462f1fea34ddadb24',
  'doctrine/cache' => 'v1.8.0@d768d58baee9a4862ca783840eca1b9add7a7f57',
  'doctrine/collections' => 'v1.6.1@d2ae4ef05e25197343b6a39bae1d3c427a2f6956',
  'doctrine/common' => 'v2.10.0@30e33f60f64deec87df728c02b107f82cdafad9d',
  'doctrine/data-fixtures' => 'v1.3.1@3a1e2c3c600e615a2dffe56d4ca0875cc5233e0a',
  'doctrine/dbal' => 'v2.9.2@22800bd651c1d8d2a9719e2a3dc46d5108ebfcc9',
  'doctrine/doctrine-bundle' => '1.10.2@1f99e6645030542079c57d4680601a4a8778a1bd',
  'doctrine/doctrine-cache-bundle' => '1.3.5@5514c90d9fb595e1095e6d66ebb98ce9ef049927',
  'doctrine/doctrine-fixtures-bundle' => '3.1.0@f016565b251c2dfa32a8d6da44d1650dc9ec1498',
  'doctrine/doctrine-migrations-bundle' => 'v1.3.2@49fa399181db4bf4f9f725126bd1cb65c4398dce',
  'doctrine/event-manager' => 'v1.0.0@a520bc093a0170feeb6b14e9d83f3a14452e64b3',
  'doctrine/inflector' => 'v1.3.0@5527a48b7313d15261292c149e55e26eae771b0a',
  'doctrine/instantiator' => '1.2.0@a2c590166b2133a4633738648b6b064edae0814a',
  'doctrine/lexer' => 'v1.0.1@83893c552fd2045dd78aef794c31e694c37c0b8c',
  'doctrine/migrations' => 'v1.8.1@215438c0eef3e5f9b7da7d09c6b90756071b43e6',
  'doctrine/orm' => 'v2.6.3@434820973cadf2da2d66e7184be370084cc32ca8',
  'doctrine/persistence' => '1.1.1@3da7c9d125591ca83944f477e65ed3d7b4617c48',
  'doctrine/reflection' => 'v1.0.0@02538d3f95e88eb397a5f86274deb2c6175c2ab6',
  'easycorp/easy-log-handler' => 'v1.0.7@5f95717248d20684f88cfb878d8bf3d78aadcbba',
  'ec-cube/plugin-installer' => '0.0.8@6185cd43ae0dd0423bebc9bb900d096356340f80',
  'egulias/email-validator' => '2.1.7@709f21f92707308cdf8f9bcfa1af4cb26586521e',
  'friendsofphp/php-cs-fixer' => 'v2.14.2@ff401e58261ffc5934a58f795b3f95b355e276cb',
  'guzzlehttp/guzzle' => '6.3.3@407b0cb880ace85c9b63c5f9551db498cb2d50ba',
  'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646',
  'guzzlehttp/psr7' => '1.5.2@9f83dded91781a01c63574e387eaa769be769115',
  'jdorn/sql-formatter' => 'v1.2.17@64990d96e0959dff8e059dfcdc1af130728d92bc',
  'justinrainbow/json-schema' => '5.2.8@dcb6e1006bb5fd1e392b4daa68932880f37550d4',
  'knplabs/knp-components' => 'v1.3.10@fc1755ba2b77f83a3d3c99e21f3026ba2a1429be',
  'knplabs/knp-paginator-bundle' => 'v2.8.0@f4ece5b347121b9fe13166264f197f90252d4bd2',
  'mobiledetect/mobiledetectlib' => '2.8.33@cd385290f9a0d609d2eddd165a1e44ec1bf12102',
  'monolog/monolog' => '1.24.0@bfc9ebb28f97e7a24c45bdc3f0ff482e47bb0266',
  'nesbot/carbon' => '1.36.2@cd324b98bc30290f233dd0e75e6ce49f7ab2a6c9',
  'nikic/php-parser' => 'v4.2.1@5221f49a608808c1e4d436df32884cbc1b821ac0',
  'ocramius/package-versions' => '1.4.0@a4d4b60d0e60da2487bd21a2c6ac089f85570dbb',
  'ocramius/proxy-manager' => '2.1.1@e18ac876b2e4819c76349de8f78ccc8ef1554cd7',
  'paragonie/random_compat' => 'v9.99.99@84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
  'php-cs-fixer/diff' => 'v1.3.0@78bb099e9c16361126c86ce82ec4405ebab8e756',
  'pimple/pimple' => 'v1.1.1@2019c145fe393923f3441b23f29bbdfaa5c58c4d',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/container' => '1.0.0@b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/log' => '1.1.0@6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd',
  'psr/simple-cache' => '1.0.1@408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
  'ralouphie/getallheaders' => '2.0.5@5601c8a83fbba7ef674a7369456d12f1e0d0eafa',
  'seld/jsonlint' => '1.7.1@d15f59a67ff805a44c50ea0516d2341740f81a38',
  'seld/phar-utils' => '1.0.1@7009b5139491975ef6486545a39f3e6dad5ac30a',
  'sensio/framework-extra-bundle' => 'v5.3.1@5f75c4658b03301cba17baa15a840b57b72b4262',
  'sensio/generator-bundle' => 'v3.1.7@28cbaa244bd0816fd8908b93f90380bcd7b67a65',
  'setasign/fpdi' => 'v2.2.0@3c266002f8044f61b17329f7cd702d44d73f0f7f',
  'setasign/fpdi-tcpdf' => 'v2.2.0@95778baea02e5da76acb9f6dc1c946be380a22d8',
  'suncat/mobile-detect-bundle' => 'v1.1.1@06007fec624587fd90e8963b796fc84fff64d4d8',
  'swiftmailer/swiftmailer' => 'v6.2.1@5397cd05b0a0f7937c47b0adcb4c60e5ab936b6a',
  'symfony/asset' => 'v3.4.26@30e3b424c4b8c5b640eb672ab57f52b8ed217124',
  'symfony/cache' => 'v3.4.26@380b8395b43f60e7d26a32f84f80c0a7ba93e7c5',
  'symfony/class-loader' => 'v3.4.26@4459eef5298dedfb69f771186a580062b8516497',
  'symfony/config' => 'v3.4.26@177a276c01575253c95cefe0866e3d1b57637fe0',
  'symfony/console' => 'v3.4.26@15a9104356436cb26e08adab97706654799d31d8',
  'symfony/css-selector' => 'v3.4.26@8ca29297c29b64fb3a1a135e71cb25f67f9fdccf',
  'symfony/debug' => 'v3.4.26@681afbb26488903c5ac15e63734f1d8ac430c9b9',
  'symfony/debug-bundle' => 'v3.4.26@04ccc8dc856fbba1ca1a325341f59e5ee6e02f73',
  'symfony/debug-pack' => 'v1.0.7@09a4a1e9bf2465987d4f79db0ad6c11cc632bc79',
  'symfony/dependency-injection' => 'v3.4.26@dee85a9148399cdb2731603802842bcfd8afe5ab',
  'symfony/doctrine-bridge' => 'v3.4.26@10d950628067b814f1b95db3693a64eb7e4fa431',
  'symfony/dom-crawler' => 'v3.4.26@d40023c057393fb25f7ca80af2a56ed948c45a09',
  'symfony/dotenv' => 'v3.4.26@7b33c7b6f497898a173e4b9d6a7698cd789d54ce',
  'symfony/event-dispatcher' => 'v3.4.26@a088aafcefb4eef2520a290ed82e4374092a6dff',
  'symfony/expression-language' => 'v3.4.26@74631d47774cfa59bfb4a0de18cdf700fb98d658',
  'symfony/filesystem' => 'v3.4.26@acf99758b1df8e9295e6b85aa69f294565c9fedb',
  'symfony/finder' => 'v3.4.26@61af5ce0b34b942d414fe8f1b11950d0e9a90e98',
  'symfony/flex' => 'v1.2.3@d65041a4c9b1dbcd606f8be3a5bae2bee4534f6a',
  'symfony/form' => 'v3.4.26@16f9919b034d4c2c80aa3b47ba3b44d7a71ac24a',
  'symfony/framework-bundle' => 'v3.4.26@172b76d504b5472761dce6a5887ed77a88eee7ec',
  'symfony/http-foundation' => 'v3.4.26@90454ad44c95d75faf3507d56388056001b74baf',
  'symfony/http-kernel' => 'v3.4.26@14fa41ccd38570b5e3120a3754bbaa144a15f311',
  'symfony/inflector' => 'v3.4.26@4a7d5c4ad3edeba3fe4a27d26ece6a012eee46b1',
  'symfony/intl' => 'v3.4.26@d2ac83703951bc3206e9ea3f6114b355de14e3ce',
  'symfony/lts' => 'v3@3a4e88df038e3197e6b66d091d2495fd7d255c0b',
  'symfony/maker-bundle' => 'v1.11.6@d262c2cace4d9bca99137a84f6fc6ba909a17e02',
  'symfony/monolog-bridge' => 'v3.4.26@8a9482e9f0900f5ebc3eae15b1ac4cf67f3db2fc',
  'symfony/monolog-bundle' => 'v3.3.1@572e143afc03419a75ab002c80a2fd99299195ff',
  'symfony/options-resolver' => 'v3.4.26@ed3b397f9c07c8ca388b2a1ef744403b4d4ecc44',
  'symfony/orm-pack' => 'v1.0.6@36c2a928482dc5f05c5c1c1b947242ae03ff1335',
  'symfony/polyfill-apcu' => 'v1.11.0@a502face1da6a53289480166f24de2c3c68e5c3c',
  'symfony/polyfill-ctype' => 'v1.11.0@82ebae02209c21113908c229e9883c419720738a',
  'symfony/polyfill-iconv' => 'v1.11.0@f037ea22acfaee983e271dd9c3b8bb4150bd8ad7',
  'symfony/polyfill-intl-icu' => 'v1.11.0@999878a3a09d73cae157b0cf89bb6fb2cc073057',
  'symfony/polyfill-intl-idn' => 'v1.11.0@c766e95bec706cdd89903b1eda8afab7d7a6b7af',
  'symfony/polyfill-mbstring' => 'v1.11.0@fe5e94c604826c35a32fa832f35bd036b6799609',
  'symfony/polyfill-php56' => 'v1.11.0@f4dddbc5c3471e1b700a147a20ae17cdb72dbe42',
  'symfony/polyfill-php70' => 'v1.11.0@bc4858fb611bda58719124ca079baff854149c89',
  'symfony/polyfill-php72' => 'v1.11.0@ab50dcf166d5f577978419edd37aa2bb8eabce0c',
  'symfony/polyfill-util' => 'v1.11.0@b46c6cae28a3106735323f00a0c38eccf2328897',
  'symfony/process' => 'v3.4.26@a9c4dfbf653023b668c282e4e02609d131f4057a',
  'symfony/profiler-pack' => 'v1.0.4@99c4370632c2a59bb0444852f92140074ef02209',
  'symfony/property-access' => 'v3.4.26@9b1c9df96a00c14445bef4cf37ad85e7239d8a4a',
  'symfony/proxy-manager-bridge' => 'v3.4.26@f534483997cdfb1984b6fb2130faa61da84771f7',
  'symfony/routing' => 'v3.4.26@ff11aac46d6cb8a65f2855687bb9a1ac9d860eec',
  'symfony/security' => 'v3.4.26@fdbff3de6a0486aa10fcc3f0cbfe336d46534f27',
  'symfony/security-bundle' => 'v3.4.26@ae2d003a6d48558bb5cde7ea2879d05a24da3ba5',
  'symfony/serializer' => 'v3.4.26@14b3221cc41dcfef404205f0060cda873f43a534',
  'symfony/stopwatch' => 'v3.4.26@2a651c2645c10bbedd21170771f122d935e0dd58',
  'symfony/swiftmailer-bundle' => 'v3.2.6@7a83160b50a2479d37eb74ba71577380b9afe4f5',
  'symfony/templating' => 'v3.4.26@1ea7458a960cf6b8818f24c62ff0db2a75835656',
  'symfony/translation' => 'v3.4.26@aae26f143da71adc8707eb489f1dc86aef7d376b',
  'symfony/twig-bridge' => 'v3.4.26@de91817e436ac4fb27c58c05d6063c9214d6f37c',
  'symfony/twig-bundle' => 'v3.4.26@57d8b5b97d754a24b2eaeb8bf02b8f7533d0df24',
  'symfony/validator' => 'v3.4.26@83da5259779aaf9dde220130e62b785f74e2ac49',
  'symfony/var-dumper' => 'v3.4.26@f0883812642a6d6583a9e2ae6aec4ba134436f40',
  'symfony/web-profiler-bundle' => 'v3.4.26@4e76752191f9a453722f379f18ef65fbdc55cee4',
  'symfony/web-server-bundle' => 'v3.4.26@71efb580e6e7d11bf1fad18b783350b1ebb21646',
  'symfony/workflow' => 'v3.4.26@d4aac685731562b9b8648bb7653d9ae3cacd8523',
  'symfony/yaml' => 'v3.4.26@212a27b731e5bfb735679d1ffaac82bd6a1dc996',
  'tecnickcom/tcpdf' => '6.2.26@367241059ca166e3a76490f4448c284e0a161f15',
  'twig/extensions' => 'v1.5.4@57873c8b0c1be51caa47df2cdb824490beb16202',
  'twig/twig' => 'v2.8.1@91cc2594d3143761ce0399c1caffd0b500ffe5b9',
  'vlucas/phpdotenv' => 'v2.4.0@3cc116adbe4b11be5ec557bf1d24dc5e3a21d18c',
  'zendframework/zend-code' => '3.3.1@c21db169075c6ec4b342149f446e7b7b724f95eb',
  'zendframework/zend-eventmanager' => '3.2.1@a5e2583a211f73604691586b8406ff7296a946dd',
  'behat/gherkin' => 'v4.6.0@ab0a02ea14893860bca00f225f5621d351a3ad07',
  'bheller/images-generator' => '1.0.1@50b61fe1dcf1b72b6a830debec4db22afd1e8ee1',
  'captbaritone/mailcatcher-codeception-module' => '1.2.1@75ba9aa803d81780ee7e9b5c36bb5b8f9139d972',
  'codeception/codeception' => '2.4.5@5fee32d5c82791548931cbc34806b4de6aa1abfc',
  'codeception/phpunit-wrapper' => '6.6.1@d0da25a98bcebeb15d97c2ad3b2de6166b6e7a0c',
  'codeception/stub' => '2.1.0@853657f988942f7afb69becf3fd0059f192c705a',
  'dama/doctrine-test-bundle' => 'v4.0.2@438346b3380cc7675e37fbcdca912fdc33471d32',
  'facebook/webdriver' => '1.6.0@bd8c740097eb9f2fc3735250fc1912bc811a954e',
  'fzaninotto/faker' => 'v1.8.0@f72816b43e74063c8b10357394b6bba8cb1c10de',
  'mikey179/vfsStream' => 'v1.6.6@095238a0711c974ae5b4ebf4c4534a23f3f6c99d',
  'myclabs/deep-copy' => '1.9.1@e6828efaba2c9b79f4499dae1d66ef8bfa7b2b72',
  'phar-io/manifest' => '1.0.1@2df402786ab5368a0169091f61a7c1e0eb6852d0',
  'phar-io/version' => '1.0.1@a70c0ced4be299a63d32fa96d9281d03e94041df',
  'php-coveralls/php-coveralls' => 'v2.1.0@3b00c229726f892bfdadeaf01ea430ffd04a939d',
  'phpdocumentor/reflection-common' => '1.0.1@21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
  'phpdocumentor/reflection-docblock' => '4.3.0@94fd0001232e47129dd3504189fa1c7225010d08',
  'phpdocumentor/type-resolver' => '0.4.0@9c977708995954784726e25d0cd1dddf4e65b0f7',
  'phpspec/prophecy' => '1.8.0@4ba436b55987b4bf311cb7c6ba82aa528aac0a06',
  'phpunit/php-code-coverage' => '5.3.2@c89677919c5dd6d3b3852f230a663118762218ac',
  'phpunit/php-file-iterator' => '1.4.5@730b01bc3e867237eaac355e06a36b85dd93a8b4',
  'phpunit/php-text-template' => '1.2.1@31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
  'phpunit/php-timer' => '1.0.9@3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
  'phpunit/php-token-stream' => '2.0.2@791198a2c6254db10131eecfe8c06670700904db',
  'phpunit/phpunit' => '6.5.14@bac23fe7ff13dbdb461481f706f0e9fe746334b7',
  'phpunit/phpunit-mock-objects' => '5.0.10@cd1cf05c553ecfec36b170070573e540b67d3f1f',
  'sebastian/code-unit-reverse-lookup' => '1.0.1@4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
  'sebastian/comparator' => '2.1.3@34369daee48eafb2651bea869b4b15d75ccc35f9',
  'sebastian/diff' => '2.0.1@347c1d8b49c5c3ee30c7040ea6fc446790e6bddd',
  'sebastian/environment' => '3.1.0@cd0871b3975fb7fc44d11314fd1ee20925fce4f5',
  'sebastian/exporter' => '3.1.0@234199f4528de6d12aaa58b612e98f7d36adb937',
  'sebastian/global-state' => '2.0.0@e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
  'sebastian/object-enumerator' => '3.0.3@7cfd9e65d11ffb5af41198476395774d4c8a84c5',
  'sebastian/object-reflector' => '1.1.1@773f97c67f28de00d397be301821b06708fca0be',
  'sebastian/recursion-context' => '3.0.0@5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
  'sebastian/resource-operations' => '1.0.0@ce990bb21759f94aeafd30209e8cfcdfa8bc3f52',
  'sebastian/version' => '2.0.1@99732be0ddb3361e16ad77b68ba41efc8e979019',
  'symfony/browser-kit' => 'v3.4.26@7f2b0843d5045468225f9a9b27a0cb171ae81828',
  'symfony/phpunit-bridge' => 'v3.4.26@a43a2f6c465a2d99635fea0addbebddc3864ad97',
  'theseer/tokenizer' => '1.1.2@1c42705be2b6c1de5904f8afacef5895cab44bf8',
  'webmozart/assert' => '1.4.0@83e253c8e0be5b0257b881e1827274667c5c17a9',
  'ec-cube/ec-cube' => 'dev-add_eccube_source@b929ca3b636687861b3bff927b9c9f91bdacce20',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException If a version cannot be located.
     */
    public static function getVersion(string $packageName) : string
    {
        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new \OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: cannot detect its version'
        );
    }
}
